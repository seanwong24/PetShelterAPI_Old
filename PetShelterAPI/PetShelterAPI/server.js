var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Services;
(function (Services) {
    var Storage = /** @class */ (function () {
        function Storage() {
        }
        Storage.GetItemList = function (dir, callback) {
            var d = [];
            this.fs.readFile(dir, "utf8", function (err, data) {
                d = JSON.parse(data);
                callback(d);
            });
        };
        Storage.AddItem = function (dir, item) {
            this.GetItemList(dir, function (result) {
                result.push(item);
                Storage.fs.writeFile(dir, JSON.stringify(result), null);
            });
        };
        Storage.fs = require("fs");
        return Storage;
    }());
    Services.Storage = Storage;
})(Services || (Services = {}));
var Controllers;
(function (Controllers) {
    var Controller = /** @class */ (function () {
        function Controller() {
        }
        Controller.prototype.init = function (app) {
            this.app = app;
            this.setActions();
        };
        ;
        Controller.prototype.get = function (route, handler) {
            this.app.get(this.route + route, handler);
        };
        ;
        Controller.prototype.put = function (route, handler) {
            this.app.put(this.route + route, handler);
        };
        ;
        Controller.prototype.post = function (route, handler) {
            this.app.post(this.route + route, handler);
        };
        ;
        Controller.prototype.delete = function (route, handler) {
            this.app.delete(this.route + route, handler);
        };
        ;
        return Controller;
    }());
    Controllers.Controller = Controller;
})(Controllers || (Controllers = {}));
var Controllers;
(function (Controllers) {
    var TestController = /** @class */ (function (_super) {
        __extends(TestController, _super);
        function TestController() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.route = "/test";
            return _this;
        }
        TestController.prototype.setActions = function () {
            this.get("", function (req, res) {
                res.end("Hello World");
            });
            this.get("/weather", function (req, res) {
                Services.Forecast.obtain(52.163056, -106.759224, function (result) {
                    res.end(result);
                });
            });
            this.get("/rain", function (req, res) {
                Services.Forecast.isRaining(52.163056, -106.759224, function (result) {
                    res.end(result.toString());
                });
            });
        };
        return TestController;
    }(Controllers.Controller));
    Controllers.TestController = TestController;
})(Controllers || (Controllers = {}));
var DomainModels;
(function (DomainModels) {
    var PetType;
    (function (PetType) {
        PetType[PetType["Dog"] = 0] = "Dog";
        PetType[PetType["Cat"] = 1] = "Cat";
    })(PetType = DomainModels.PetType || (DomainModels.PetType = {}));
    var Pet = /** @class */ (function () {
        function Pet() {
        }
        return Pet;
    }());
    DomainModels.Pet = Pet;
})(DomainModels || (DomainModels = {}));
/// <reference path="./Pet.ts" /> 
/// <reference path="../Services/_.ts" />
/// <reference path="../DomainModels/_.ts" />
var Controllers;
(function (Controllers) {
    var PetController = /** @class */ (function (_super) {
        __extends(PetController, _super);
        function PetController() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.route = "/pet";
            return _this;
        }
        PetController.prototype.setActions = function () {
            this.get("", function (req, res) {
                Services.Storage.GetItemList("./Data/Pets.json", function (result) {
                    res.end(JSON.stringify(result));
                });
            });
            this.get("/add", function (req, res) {
                var pet = JSON.parse(req.body);
                Services.Storage.AddItem("./Data/Pets.json", pet);
            });
            this.get("/:id", function (req, res) {
                Services.Storage.GetItemList("./Data/Pets.json", function (result) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].id == req.params.id) {
                            res.end(JSON.stringify(result[i]));
                            break;
                        }
                    }
                });
            });
        };
        return PetController;
    }(Controllers.Controller));
    Controllers.PetController = PetController;
})(Controllers || (Controllers = {}));
/// <reference path="./Controller.ts" />
/// <reference path="./TestController.ts" />
/// <reference path="./PetController.ts" /> 
/// <reference path="../Controllers/_.ts" />
var Services;
(function (Services) {
    var ServerHost = /** @class */ (function () {
        function ServerHost() {
            this.express = require("express");
            this.app = this.express();
        }
        ServerHost.prototype.addController = function (controller) {
            controller.init(this.app);
            return this;
        };
        ServerHost.prototype.start = function (port) {
            this.app.listen(port);
        };
        return ServerHost;
    }());
    Services.ServerHost = ServerHost;
})(Services || (Services = {}));
var Services;
(function (Services) {
    var Forecast = /** @class */ (function () {
        function Forecast() {
        }
        Forecast.obtain = function (latitude, longitude, callback) {
            var uri = "https://api.darksky.net/forecast/5cff5ee8cfdf80455d2238fe553161c9"
                + "/" + latitude + "," + longitude;
            this.request(uri, function (error, response, body) {
                callback(body);
            });
        };
        Forecast.isRaining = function (latitude, longitude, callback) {
            this.obtain(latitude, longitude, function (r) {
                callback(JSON.parse(r).currently.icon == "rain");
            });
        };
        Forecast.request = require("request");
        return Forecast;
    }());
    Services.Forecast = Forecast;
})(Services || (Services = {}));
/// <reference path="./Storage.ts" />
/// <reference path="./ServerHost.ts" />
/// <reference path="./Forecast.ts" /> 
/// <reference path="./Services/_.ts" />
/// <reference path="./DomainModels/_.ts" />
var port = 10086;
new Services.ServerHost()
    .addController(new Controllers.TestController())
    .addController(new Controllers.PetController())
    .start(10086);
