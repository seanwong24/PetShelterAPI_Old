/// <reference path="./Services/_.ts" />
/// <reference path="./DomainModels/_.ts" />

var port = 10086;

new Services.ServerHost()
    .addController(new Controllers.TestController())
    .addController(new Controllers.PetController())
    .start(10086);