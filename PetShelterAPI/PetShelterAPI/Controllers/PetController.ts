﻿/// <reference path="../Services/_.ts" />
/// <reference path="../DomainModels/_.ts" />

namespace Controllers {
    export class PetController extends Controller {
        protected route: string = "/pet";
        setActions() {
            this.get("", function (req, res) {
                Services.Storage.GetItemList<DomainModels.Pet>("./Data/Pets.json", function (result) {
                    res.end(JSON.stringify(result));
                });
            })

            this.get("/add", function (req, res) {
                var pet: DomainModels.Pet = JSON.parse(req.body);
                Services.Storage.AddItem("./Data/Pets.json" ,pet);
            })
            
            this.get("/:id", function (req, res) {
                Services.Storage.GetItemList<DomainModels.Pet>("./Data/Pets.json", function (result) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].id == req.params.id) {
                            res.end(JSON.stringify(result[i]));
                            break;
                        }
                    }
                });
            })
        }

    }
}