﻿namespace Controllers {
    export class TestController extends Controller {
        protected route: string = "/test";
        setActions() {
            this.get("", function (req, res) {
                res.end("Hello World");
            })

            this.get("/weather", function (req, res) {
                Services.Forecast.obtain(52.163056, -106.759224, function (result) {
                    res.end(result);
                })
            })

            this.get("/rain", function (req, res) {
                Services.Forecast.isRaining(52.163056, -106.759224, function (result) {
                    res.end(result.toString());
                })
            })
        }
    }
}