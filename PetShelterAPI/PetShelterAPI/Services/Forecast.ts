﻿namespace Services {
    export class Forecast {
        static request = require("request");
        static obtain(latitude: number, longitude: number, callback: (result: string) => void) {
            var uri = "https://api.darksky.net/forecast/5cff5ee8cfdf80455d2238fe553161c9"
                + "/" + latitude + "," + longitude
            this.request(uri, function (error, response, body) {
                callback(body);
            })
        }
        static isRaining(latitude: number, longitude: number, callback: (result: boolean) => void) {
            this.obtain(latitude, longitude, function (r) {
                callback(JSON.parse(r).currently.icon == "rain");
            })
        }
    }
}