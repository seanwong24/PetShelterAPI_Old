﻿namespace Services {
    export class Storage {
        private static fs = require("fs");

        static GetItemList<T>(dir: string, callback: (result: Array<T>) => void) {
            var d: Array<T> = [];
            this.fs.readFile(dir, "utf8", function (err, data) {
                d = JSON.parse(data);
                callback(d);
            });
        }

        static AddItem<T>(dir: string, item: T) {
            this.GetItemList<T>(dir, function (result) {
                result.push(item);
                Storage.fs.writeFile(dir, JSON.stringify(result), null);
            });
        }
    }
}