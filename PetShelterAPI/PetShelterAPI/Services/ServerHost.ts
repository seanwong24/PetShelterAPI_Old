﻿/// <reference path="../Controllers/_.ts" />

namespace Services {
    export class ServerHost {
        express;
        app;

        constructor() {
            this.express = require("express");
            this.app = this.express();
        }

        addController(controller: Controllers.Controller): ServerHost {
            controller.init(this.app);
            return this;
        }

        start(port: number) {
            this.app.listen(port);
        }
    }
}