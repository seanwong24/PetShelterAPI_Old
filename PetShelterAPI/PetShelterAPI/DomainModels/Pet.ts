﻿namespace DomainModels {
    export enum PetType {
        Dog = 0,
        Cat = 1
    }
    export class Pet {
        id: number;
        name: string;
        type: PetType;
        breed: string;
        location: string;
        latitude: number;
        longitude: number;
    }
}